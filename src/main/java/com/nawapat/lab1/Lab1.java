/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 */
package com.nawapat.lab1;

import java.util.Scanner;

/**
 *
 * @author asus
 */
public class Lab1 {

    public static void main(String[] args) {
        System.out.println("Welcome to OX program");
        Scanner input = new Scanner(System.in);
        int[][] board = {
            {0, 0, 0},
            {0, 0, 0},
            {0, 0, 0}
        };
        
        System.out.println("!! Start !!");

        drawBoard(board);
        int row, column;
        for (int i = 0; i < 9; i++) {

            while (true) {
                System.out.println("Turn " + (i % 2 == 0 ? "X" : "O"));
                System.out.print("Please input row : ");
                row = input.nextInt() - 1;
                System.out.println("Turn " + (i % 2 == 0 ? "X" : "O"));
                System.out.print("Please input column : ");
                column = input.nextInt() - 1;
                if (board[row][column] != 0) {
                    System.out.println("Row " + row + " and Column " + column
                            + " is already taken, try again");
                } else {
                    break;
                }
            }
            board[row][column] = (i % 2 == 0) ? 1 : 2;
            drawBoard(board);

            switch (checkStatus(board)) {
                case 1:
                    System.out.println("X player wins!");
                    return;
                case 2:
                    System.out.println("O player wins!");
                    return;
            }
        }
        System.out.println("It's a draw!");
    }

    public static void drawBoard(int[][] board) {
        for (int[] row : board) {
            for (int cell : row) {
                switch (cell) {
                    case 0:
                        System.out.print("| ");
                        break;
                    case 1:
                        System.out.print("|X");
                        break;
                    case 2:
                        System.out.print("|O");
                        break;
                    default:
                        System.out.println("Wrong element on board");
                        System.exit(1);
                }
            }
            System.out.println("|");
        }
    }

    public static int checkStatus(int[][] board) {
        if (isFormLine(board, 1)) {
            return 1;
        }
        if (isFormLine(board, 2)) {
            return 2;
        }
        return 0;
    }

    public static boolean isFormLine(int[][] board, int number) {
        boolean lineFormed;

        for (int i = 0; i < board.length; i++) {
            lineFormed = true;
            for (int j = 1; j < board[i].length; j++) {
                if (board[i][j - 1] != number || board[i][j] != number) {
                    lineFormed = false;
                }
            }
            if (lineFormed) {
                return true;
            }
        }
        
        for (int j = 0; j < board[0].length; j++) {
            lineFormed = true;
            for (int i = 1; i < board.length; i++) {
                if (board[i - 1][j] != number || board[i][j] != number) {
                    lineFormed = false;
                }
            }
            if (lineFormed) {
                return true;
            }
        }
        
        lineFormed = true;
        for (int i = 1; i < board.length; i++) {
            if (board[i - 1][i - 1] != number || board[i][i] != number) {
                lineFormed = false;
            }
        }
        if (lineFormed) {
            return true;
        }

        lineFormed = true;
        for (int i = 1; i < board.length; i++) {
            if (board[board.length - i][i - 1] != number
                    || board[board.length - i - 1][i] != number) {
                lineFormed = false;
            }
        }
        if (lineFormed) {
            return true;
        }
        return false;
    }
}
